﻿#ifndef MODEL_H
#define MODEL_H

#include "listmodel.h"

class Ingredient: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(int count1 READ count1 WRITE setCount1 NOTIFY count1Changed)


public:
    explicit Ingredient(QObject *parent = nullptr);
    Ingredient(const int &id, const QString &name,const int &count1 , QObject * parent = nullptr);

    QVariant data(int role) const;
    bool setData(const QVariant &v, int role);

    int id() const;
    QString name() const;
    int count1() const;
    void setId(int id);
    void setName(QString name);
    void setCount1(int count1);

signals:
    void idChanged(int id);
    void nameChanged(QString name);
    void count1Changed(int count1);

private:
    QString m_name;
    int m_id;
    int m_count1;
};

class IngredientModel : public EditableListModel<Ingredient*>
{
    Q_OBJECT

public:
    enum Roles {
        ID = Qt::UserRole + 1,
        Name,
        Count
    };
    Q_ENUM(Roles)

    explicit IngredientModel(QObject *parent = nullptr);
};

class Recipe: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(int count READ count WRITE setCount NOTIFY countChanged)


public:
    explicit Recipe(QObject *parent = nullptr);
    Recipe(const int &id, const QString &description,const int &count , QObject * parent = nullptr);

    IngredientModel *ingredient = nullptr;

    QVariant data(int role) const;
    bool setData(const QVariant &v, int role);

    int id() const;
    QString description() const;
    int count() const;
    void setId(int id);
    void setDescription(QString description);
    void setCount(int count);

signals:
    void idChanged(int id);
    void descriptionChanged(QString description);
    void countChanged(int count);


private:
    QString m_description;
    int m_id;
    int m_count;

};

class RecipeModel : public EditableListModel<Recipe*>
{
    Q_OBJECT

public:
    enum Roles {
        ID = Qt::UserRole + 1,
        Description,
        Count,
        Ingredient
    };
    Q_ENUM(Roles)

    explicit RecipeModel(QObject *parent = nullptr);
};

class Category : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    explicit Category(QObject *parent = nullptr);
    Category(const QString &name, QObject * parent = nullptr);

    RecipeModel *recipes = nullptr;

    QVariant data(int role) const;
    bool setData(const QVariant &v, int role);

    QString name() const;
    void setName(QString name);

signals:

    void nameChanged(QString name);

private:

    QString m_name;
};

class CategoryModel : public EditableListModel<Category*>
{
    Q_OBJECT

public:
    enum Roles {
        Name = Qt::UserRole + 1,
        Recipe
    };
    Q_ENUM(Roles)

    explicit CategoryModel(QObject *parent = nullptr);
};

#endif // MODEL_H
