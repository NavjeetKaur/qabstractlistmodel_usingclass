﻿#ifndef LISTMODEL_H
#define LISTMODEL_H

#include <QMetaEnum>
#include <QAbstractListModel>
#include <QDebug>

template <class T>
class ListModel : public QAbstractListModel
{
public:
    ListModel(QObject *parent = nullptr)
        : QAbstractListModel(parent)
    {}

    ~ListModel()
    {}

    enum Roles {};
    Q_ENUM(Roles)

    QHash<int, QByteArray> roleNames() const Q_DECL_FINAL
    {
        // Builds the roles names from the Roles enum
        QHash<int, QByteArray> roles;
        const int enumIdx = metaObject()->indexOfEnumerator("Roles");
        const QMetaEnum metaEnum = metaObject()->enumerator(enumIdx);
        const int keysCount = metaEnum.keyCount();

        for (int i = 0; i < keysCount; ++i)
            roles.insert(metaEnum.value(i), metaEnum.key(i));

        return roles;
    }

    QVariant data(const QModelIndex &index, int role) const Q_DECL_FINAL
    {
        if (index.row() < int(m_items.size())) {
            return m_items.at(index.row())->data(role);
        }
        return QVariant();
    }

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_FINAL
    {
        Q_UNUSED(parent);
        return int(m_items.size());
    }

    bool insertRows(const std::vector<T> &data)
    {
        if (data.size() == 0)
            return false;
        beginInsertRows(QModelIndex(), m_items.size(), m_items.size() + data.size() - 1);
        m_items.insert(m_items.end(), data.begin(), data.end());
        endInsertRows();
        return true;
    }

    bool insertRows(std::vector<T> &&data)
    {
        if (data.size() == 0)
            return false;
        qDebug() << Q_FUNC_INFO;
        beginInsertRows(QModelIndex(), m_items.size(), m_items.size() + data.size() - 1);
        std::move(data.begin(), data.end(), std::back_inserter(m_items));
        endInsertRows();
        return true;
    }

    bool insertRow(const T &data)
    {
        beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
        m_items.push_back(data);
        endInsertRows();
        return true;
    }

    bool insertRow(T &&data)
    {
        beginInsertRows(QModelIndex(), m_items.size(), m_items.size());
        m_items.push_back(std::move(data));
        endInsertRows();
        return true;
    }

    Q_INVOKABLE bool removeRows(int row, int count = 0)
    {
        beginRemoveRows(QModelIndex(), row, row + count);
        m_items.erase(m_items.begin() + row, m_items.begin() + row + count + 1);
        endRemoveRows();
        return true;
    }

    bool clear()
    {
        return removeRows(0, rowCount() - 1);
    }

    std::vector<T> &items()
    {
        return m_items;
    }

protected:
    std::vector<T> m_items;
};

template <class T>
class EditableListModel : public ListModel<T>
{
public:
    EditableListModel(QObject *parent = nullptr)
        : ListModel<T>(parent)
    {
        qDebug() << Q_FUNC_INFO;
    }

    bool setData(const QModelIndex &index, const QVariant &value, int role) Q_DECL_FINAL
    {
        if (index.row() < ListModel<T>::m_items.size()) {
            if (ListModel<T>::m_items.at(index.row())->setData(value, role)) {
                emit QAbstractListModel::dataChanged(index, index, (QVector<int>() << role));
                return true;
            }
        }
        return false;
    }

};

#endif // LISTMODEL_H
