import QtQuick 2.12
import QtQuick.Window 2.12

import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.3

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    property bool nameRecieved: false

    Component {
        id: categoryDelegate
        Rectangle {
            height: name.implicitHeight + (recipe.visible ? recipe.height : 0)
            readonly property variant recipeModel: model.Recipe
            id:rect
            width: 450
            border.color: "black"
            //color: colorCode
            Text {
                id: name
                text: model.Name
            }
            MouseArea{
                anchors.fill: parent
                onClicked: recipe.visible=!recipe.visible
            }
            ListView
            {
                id:recipe
                height: 200
                visible: false
                anchors.top: name.bottom
                anchors.left: parent.left
                anchors.leftMargin: 25
                anchors.right: parent.right
                model: recipeModel
                delegate: recipeDelegate
                clip: true
            }
        }
    }

    Component {
        id: recipeDelegate
        Item {
            id: recipeCompDelegate
            readonly property variant ingredientModel: model.Ingredient
            onIngredientModelChanged: {
                if(ingredientModel !== null){
                    nameRecieved = true
                    console.log("nameRecieved" +nameRecieved)
                }
            }

            width: innerRow.implicitWidth
            height: innerRow.height + (ingredient.visible ? ingredient.height : 0)
            Row{
                id: innerRow
                height: Math.max(txt_Recipe.implicitHeight, mSpinbox.implicitHeight)
                Text {
                    id: txt_Recipe
                    height: 10
                    text: model.Description
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            mSpinbox.visible = !mSpinbox.visible
                        }
                    }
                }
                SpinBox{
                    id : mSpinbox
                    editable: true
                    value: model.Count
                    visible: false
                    // Note: we use onValueModified instead of onValueChanged to avoid a binding loop
                    onValueModified: {
                        model.Count = value;
                        //                        model.returnModel();
                        //console.log("Name in Ingredient" +model.Count)
                        //console.log("Name in Ingredient" +txt_Ingredient.text)
                        //console.log("model"+model+"Recipemodel"+recipeModel+"iNGREDIENTMODE"+ingredientModel)
                    }
                }
            }
            ListView
            {
                id: ingredient
                height: contentHeight + 20
                visible: mSpinbox.visible
                enabled: visible
                anchors.top: innerRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 25
                model: ingredientModel
                delegate: ingredientDelegate
            }
        }
    }

    Component {
        id: ingredientDelegate
        Item {
            visible: true
            height: innerRow.height
            width: innerRow.implicitWidth
            Row{
                id: innerRow
                height: Math.max(txt_Ingredient.implicitHeight, mSpinbox1.implicitHeight)
                Text {
                    id: txt_Ingredient
                    height: 10
                    text: model.Name //: "ABC"// nameRecieved ? model.Name : "no Value"
                    MouseArea{
                        id:ma_Ing
                        anchors.fill: parent
                        onClicked:{
                            mSpinbox1.visible = !mSpinbox1.visible
                            console.log("model.Name " +model.Name)
                        }
                    }
                }
                SpinBox{
                    id : mSpinbox1
                    height: 20
                    visible: false
                    editable: true
                    value: model.Count
                    onValueModified: {
                        model.Count = value;
                        console.log("Name in Ingredient###" +model.Name)

                    }
                }
            }
        }
    }

    ListView {
        id: listView
        x: 97
        y: 143
        width: 110
        height: 400
        delegate: categoryDelegate
        model: myModel
    }
}
