#include "models.h"

RecipeModel::RecipeModel(QObject *parent)
    : EditableListModel<Recipe*>(parent)
{
}

Recipe::Recipe(QObject *parent) : QObject(parent)
{

}

Recipe::Recipe(const int &id, const QString &description, const int &count, QObject *parent):
    QObject(parent),m_id(id),m_description(description),m_count(count)
{

}

int Recipe::id() const
{
    return m_id;
}

QString Recipe::description() const
{
    return m_description;
}

int Recipe::count() const
{
    return m_count;
}

void Recipe::setDescription(QString description)
{
    if (m_description == description)
        return;

    m_description = description;
    emit descriptionChanged(m_description);
}

void Recipe::setId(int id)
{
    qDebug() << "Favorite color called";
    if (m_id == id)
        return;

    m_id = id;
    emit idChanged(m_id);
}

void Recipe::setCount(int count)
{
    if (m_count == count)
        return;

    m_count = count;
    emit countChanged(m_count);
}


QVariant Recipe::data(int role) const
{
    switch (role) {
    case RecipeModel::ID:
        return m_id;
    case RecipeModel::Description:
        return m_description;
    case RecipeModel::Count:
        return m_count;
    case RecipeModel::Ingredient:
        qDebug() << Q_FUNC_INFO << ingredient;
        return QVariant::fromValue(static_cast<QObject *>(ingredient));
    default:
        return QVariant();
    }
}

bool Recipe::setData(const QVariant &v, int role)
{
    switch (role) {
    case RecipeModel::ID:
        m_id = v.toInt();
        return true;
    case RecipeModel::Description:
        m_description = v.toString();
        return true;
    case RecipeModel::Count:
        m_count = v.toInt();
        return true;
    default:
        return false;
    }
}


QVariant Category::data(int role) const
{
    switch (role) {
    case CategoryModel::Name:
        return m_name;
    case CategoryModel::Recipe:
        qDebug() << Q_FUNC_INFO << recipes;
        return QVariant::fromValue(static_cast<QObject *>(recipes));
    default:
        return QVariant();
    }
}

bool Category::setData(const QVariant &v, int role)
{
    switch (role) {
    case CategoryModel::Name:
        m_name = v.toString();
        return true;
    default:
        return false;
    }
}

CategoryModel::CategoryModel(QObject *parent)
    : EditableListModel<Category*>(parent)
{
}

Category::Category(QObject *parent) : QObject(parent)
{

}

//Category::Category(const QString &name, QObject *parent):
//    QObject(parent),m_name(name)
//{

//}

QString Category::name() const
{
    return m_name;
}

void Category::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

IngredientModel::IngredientModel(QObject *parent)
    : EditableListModel<Ingredient*>(parent)
{
    qDebug() << Q_FUNC_INFO;
}

Ingredient::Ingredient(QObject *parent) : QObject(parent)
{

}

Ingredient::Ingredient(const int &id, const QString &name, const int &count1, QObject *parent):
    QObject(parent),m_id(id),m_name(name),m_count1(count1)
{

}

int Ingredient::id() const
{
    return m_id;
}

QString Ingredient::name() const
{
    return m_name;
}

int Ingredient::count1() const
{
    return m_count1;
}

void Ingredient::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

void Ingredient::setId(int id)
{
    if (m_id == id)
        return;

    m_id = id;
    emit idChanged(m_id);
}

void Ingredient::setCount1(int count1)
{
    if (m_count1 == count1)
        return;

    m_count1 = count1;
    emit count1Changed(m_count1);
}


QVariant Ingredient::data(int role) const
{
    qDebug() << Q_FUNC_INFO << role << "*****" << m_name << m_id << m_count1;
    switch (role) {
    case IngredientModel::ID:
        return m_id;
    case IngredientModel::Name:
        qDebug() << Q_FUNC_INFO << m_name;
        return m_name;
    case IngredientModel::Count:
        return m_count1;
    default:
        return QVariant();
    }
}

bool Ingredient::setData(const QVariant &v, int role)
{
    switch (role) {
    case IngredientModel::ID:
        m_id = v.toInt();
        return true;
    case IngredientModel::Name:
        m_name = v.toString();
        return true;
    case IngredientModel::Count:
        m_count1 = v.toInt();
        return true;
    default:
        return false;
    }
}
