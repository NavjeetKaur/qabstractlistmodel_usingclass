#include <QQuickView>
#include <QString>
#include <QGuiApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include "models.h"
#include <QStringLiteral>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    // Fill out data model here
    CategoryModel myModel;

    Category p1;
    p1.setName(QStringLiteral("Category 1"));
    p1.recipes = new RecipeModel(&myModel);

    Recipe t11;
    t11.setId(1);
    t11.setDescription(QStringLiteral("Cheese Cake"));
    t11.setCount(20);
    t11.ingredient = new IngredientModel(&myModel);


    Ingredient i11;
    i11.setName(QStringLiteral("Cheese"));
    i11.setId(1);
    i11.setCount1(5);

    Ingredient i12;
    i12.setName(QStringLiteral("Sugar"));
    i12.setId(2);
    i12.setCount1(10);

    Ingredient i13;
    i13.setName(QStringLiteral("Cream"));
    i13.setId(3);
    i13.setCount1(15);

    t11.ingredient->insertRow(&i11);
    t11.ingredient->insertRow(&i12);
    t11.ingredient->insertRow(&i13);

    p1.recipes->insertRow(&t11);

    Recipe t12;
    t12.setId(2);
    t12.setDescription(QStringLiteral("Almond Cake"));
    t12.setCount(10);

    t12.ingredient = new IngredientModel(&myModel);

    Ingredient i21;
    i21.setId(1);
    i21.setName(QStringLiteral("Cheese"));
    i21.setCount1(5);

    Ingredient i22;
    i22.setId(2);
    i22.setName(QStringLiteral("Sugar"));
    i22.setCount1(10);

    Ingredient i23;
    i23.setId(3);
    i23.setName(QStringLiteral("Cream"));
    i23.setCount1(15);

    t12.ingredient->insertRow(&i21);
    t12.ingredient->insertRow(&i22);
    t12.ingredient->insertRow(&i23);
    p1.recipes->insertRow(&t12);

    Recipe t13;
    t13.setId(3);
    t13.setDescription("Fruit Cake");
    t13.setCount(13);
    p1.recipes->insertRow(&t13);

    Recipe t14;
    t14.setId(4);
    t14.setDescription( QStringLiteral("Chocolate Cake"));
    t14.setCount(14);
    p1.recipes->insertRow(&t14);

    myModel.insertRow(&p1);

    Category p2;
    p2.setName(QStringLiteral("Category 2"));
    p2.recipes = new RecipeModel(&myModel);

    Recipe t21;
    t21.setId(1);
    t21.setDescription(QStringLiteral("Cheese Roll"));
    t21.setCount(11);
    p2.recipes->insertRow(&t21);

    Recipe t22;
    t22.setId(2);
    t22.setDescription(QStringLiteral("Finger Chips"));
    t22.setCount(50);
    p2.recipes->insertRow(&t22);

    myModel.insertRow(&p2);

    QQmlApplicationEngine engine;

    //QQmlContext *context = engine.rootContext();

    //context->setContextProperty("myModel", &myModel);

    engine.rootContext()->setContextProperty("myModel", &myModel);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
